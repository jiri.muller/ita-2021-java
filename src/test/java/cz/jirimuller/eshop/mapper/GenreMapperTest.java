package cz.jirimuller.eshop.mapper;

import cz.jirimuller.eshop.domain.Genre;
import cz.jirimuller.eshop.model.GenreDto;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.assertj.core.api.Assertions.assertThat;

public class GenreMapperTest {

    private GenreMapper genreMapper = Mappers.getMapper(GenreMapper.class);

    @Test
    void mapToDto() {
        // arrange
        final Genre genre = (Genre) new Genre()
                .setName("Zanr")
                .setDescription("To je zanr a zanr ja respektuji")
                .setId(1L);

        // act
        final GenreDto response = genreMapper.mapToDto(genre);

        // assert
        assertThat(response).isNotNull();
        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(response.getName()).isEqualTo(genre.getName());
            softAssertions.assertThat(response.getDescription()).isEqualTo(genre.getDescription());
            softAssertions.assertThat(response.getId()).isEqualTo(genre.getId());
        });
    }

    @Test
    void mapToDto_nullInput() {
        // arrange & act
        final GenreDto genre = genreMapper.mapToDto(null);

        // assert
        assertThat(genre).isNull();
    }
}
