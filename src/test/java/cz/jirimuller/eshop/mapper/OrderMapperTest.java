package cz.jirimuller.eshop.mapper;

import cz.jirimuller.eshop.domain.Order;
import cz.jirimuller.eshop.domain.OrderStatus;
import cz.jirimuller.eshop.domain.Product;
import cz.jirimuller.eshop.model.OrderDto;
import cz.jirimuller.eshop.model.OrderRequestDto;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class OrderMapperTest {

    private OrderMapper orderMapper = Mappers.getMapper(OrderMapper.class);

    @Test
    void mapToEntity() {
        // arrange
        final OrderRequestDto request = new OrderRequestDto()
                .setName("Name")
                .setAddress("Street");

        // act
        final Order order = orderMapper.mapToEntity(request);

        // assert
        assertThat(order).isNotNull();
        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(order.getName()).isEqualTo(request.getName());
            softAssertions.assertThat(order.getAddress()).isEqualTo(request.getAddress());
            });
    }

    @Test
    void mapToEntity_nullInput() {
        // arrange & act
        final Order order = orderMapper.mapToEntity(null);

        // assert
        assertThat(order).isNull();
    }

    @Test
    void mapToDto() {
        // arrange
        final Product product = (Product) new Product()
                .setDescription("A test product")
                .setStock(2L)
                .setPrice(BigDecimal.valueOf(22))
                .setImage("prod.jpg")
                .setName("Testprdt")
                .setId(1L);

        final Order order = (Order) new Order()
                .setAddress("Hello World street 12")
                .setName("Order in the court")
                .setStatus(OrderStatus.NEW)
                .setProducts(List.of(product))
                .setId(1L);

        // act
        final OrderDto response = orderMapper.mapToDto(order);

        // assert
        assertThat(response).isNotNull();
        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(response.getAddress()).isEqualTo(order.getAddress());
            softAssertions.assertThat(response.getName()).isEqualTo(order.getName());
            softAssertions.assertThat(response.getStatus()).isEqualTo(OrderStatus.NEW);
            softAssertions.assertThat(response.getProducts()).isEqualTo(order.getProducts());
            softAssertions.assertThat(response.getId()).isEqualTo(order.getId());
        });
    }

    @Test
    void mapToDto_nullInput() {
        // arrange & act
        final OrderDto order = orderMapper.mapToDto(null);

        // assert
        assertThat(order).isNull();
    }
}