package cz.jirimuller.eshop.mapper;

import cz.jirimuller.eshop.domain.Cart;
import cz.jirimuller.eshop.domain.Product;
import cz.jirimuller.eshop.model.CartDto;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CartMapperTest {

    private CartMapper cartMapper = Mappers.getMapper(CartMapper.class);
    
    @Test
    void mapToDto() {
        // arrange
        final Product product = new Product()
                .setDescription("Description")
                .setImage("image.jpg")
                .setName("Name")
                .setStock(42L)
                .setPrice(BigDecimal.valueOf(124L));
        
        final Cart cart = (Cart) new Cart()
                .setProducts(List.of(product))
                .setId(1L);

        // act
        final CartDto response = cartMapper.mapToDto(cart);

        // assert
        assertThat(response).isNotNull();
        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(response.getId()).isEqualTo(cart.getId());
            softAssertions.assertThat(response.getProducts()).isEqualTo(cart.getProducts());
        });
    }

    @Test
    void mapToDto_nullInput() {
        // arrange & act
        final CartDto cart = cartMapper.mapToDto(null);

        // assert
        assertThat(cart).isNull();
    }
}
