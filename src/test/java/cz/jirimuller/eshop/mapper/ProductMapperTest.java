package cz.jirimuller.eshop.mapper;

import cz.jirimuller.eshop.domain.Product;
import cz.jirimuller.eshop.model.ProductDto;
import cz.jirimuller.eshop.model.ProductRequestDto;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class ProductMapperTest {

    private ProductMapper productMapper = Mappers.getMapper(ProductMapper.class);

    @Test
    void mapToEntity() {
        // arrange
        final ProductRequestDto request = new ProductRequestDto()
                .setDescription("Description")
                .setImage("image.jpg")
                .setName("Name")
                .setStock(42L)
                .setPrice(BigDecimal.valueOf(124L));

        // act
        final Product product = productMapper.mapToEntity(request);

        // assert
        assertThat(product).isNotNull();
        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(product.getDescription()).isEqualTo(request.getDescription());
            softAssertions.assertThat(product.getImage()).isEqualTo(request.getImage());
            softAssertions.assertThat(product.getName()).isEqualTo(request.getName());
            softAssertions.assertThat(product.getStock()).isEqualTo(request.getStock());
            softAssertions.assertThat(product.getPrice()).isEqualTo(request.getPrice());
        });
    }

    @Test
    void mapToEntity_nullInput() {
        // arrange & act
        final Product product = productMapper.mapToEntity(null);

        // assert
        assertThat(product).isNull();
    }

    @Test
    void mapToDto() {
        // arrange
        final Product product = new Product()
                .setDescription("Description")
                .setImage("image.jpg")
                .setName("Name")
                .setStock(42L)
                .setPrice(BigDecimal.valueOf(124L));

        // act
        final ProductDto response = productMapper.mapToDto(product);

        // assert
        assertThat(response).isNotNull();
        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(response.getDescription()).isEqualTo(product.getDescription());
            softAssertions.assertThat(response.getImage()).isEqualTo(product.getImage());
            softAssertions.assertThat(response.getName()).isEqualTo(product.getName());
            softAssertions.assertThat(response.getStock()).isEqualTo(product.getStock());
            softAssertions.assertThat(response.getPrice()).isEqualTo(product.getPrice());
        });
    }

    @Test
    void mapToDto_nullInput() {
        // arrange & act
        final ProductDto product = productMapper.mapToDto(null);

        // assert
        assertThat(product).isNull();
    }
}