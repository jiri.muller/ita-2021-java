package cz.jirimuller.eshop.mapper;

import cz.jirimuller.eshop.domain.Author;
import cz.jirimuller.eshop.model.AuthorDto;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.assertj.core.api.Assertions.assertThat;

public class AuthorMapperTest {

    private AuthorMapper authorMapper = Mappers.getMapper(AuthorMapper.class);

    @Test
    void mapToDto() {
        // arrange
        final Author author = (Author) new Author()
                .setName("An Author")
                .setBio("This guy is amazing")
                .setId(1L);

        // act
        final AuthorDto response = authorMapper.mapToDto(author);

        // assert
        assertThat(response).isNotNull();
        SoftAssertions.assertSoftly(softAssertions -> {
            softAssertions.assertThat(response.getId()).isEqualTo(author.getId());
            softAssertions.assertThat(response.getBio()).isEqualTo(author.getBio());
            softAssertions.assertThat(response.getName()).isEqualTo(author.getName());
        });
    }

    @Test
    void mapToDto_nullInput() {
        // arrange & act
        final AuthorDto author = authorMapper.mapToDto(null);

        // assert
        assertThat(author).isNull();
    }
}
