package cz.jirimuller.eshop.service.impl;

import cz.jirimuller.eshop.domain.Cart;
import cz.jirimuller.eshop.domain.Order;
import cz.jirimuller.eshop.domain.OrderStatus;
import cz.jirimuller.eshop.domain.Product;
import cz.jirimuller.eshop.exception.ItaCartNotFoundException;
import cz.jirimuller.eshop.exception.ItaProductNotFoundException;
import cz.jirimuller.eshop.mapper.CartMapper;
import cz.jirimuller.eshop.mapper.OrderMapper;
import cz.jirimuller.eshop.model.OrderDto;
import cz.jirimuller.eshop.model.OrderRequestDto;
import cz.jirimuller.eshop.repository.CartRepository;
import cz.jirimuller.eshop.repository.OrderRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest implements WithAssertions {

    @Mock
    private CartRepository cartRepository;

    @Spy
    private OrderRepository orderRepository;

    @Spy
    private OrderMapper orderMapper;

    @Spy
    private CartMapper cartMapper;

    @InjectMocks
    private OrderServiceImpl service;

    @Test
    void createOrder() {
        // arrange
        final Product product0 = new Product();
        final Product product1 = new Product();
        final Product product2 = new Product();
        final List<Product> products = List.of(product0, product1, product2);

        final Cart cart = new Cart()
                .setProducts(products);

        final OrderRequestDto request = new OrderRequestDto()
                .setIdCart(1L);
        final Order order = new Order();
        final OrderDto response = new OrderDto();

        when(cartRepository.findById(1L))
                .thenReturn(Optional.of(cart));
        when(orderMapper.mapToEntity(request))
                .thenReturn(order);
        when(orderRepository.save(order))
                .thenReturn(order);
        when(orderMapper.mapToDto(order))
                .thenReturn(response);

        // act
        final OrderDto result = service.createOrder(request);

        // assert
        assertThat(result).isNotNull();
        verify(cartRepository).findById(1L);
        verify(orderMapper).mapToEntity(request);
        verify(orderRepository).save(order);
        verify(orderMapper).mapToDto(order);
        verifyNoMoreInteractions(cartRepository, cartMapper, orderRepository, orderMapper);
    }

    @Test
    void createOrder_cartNotFound() {
        // arrange
        final OrderRequestDto request = new OrderRequestDto()
                .setIdCart(1L);

        when(cartRepository.findById(1L))
                .thenThrow(new ItaCartNotFoundException(1L));

        // act & assert
        final ItaCartNotFoundException expectedException = assertThrows(ItaCartNotFoundException.class, () -> service.createOrder(request));
        assertThat(expectedException.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(cartRepository).findById(1L);
        verifyNoMoreInteractions(cartMapper, cartRepository);
    }
}
