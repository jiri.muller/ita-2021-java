package cz.jirimuller.eshop.service.impl;

import cz.jirimuller.eshop.domain.Genre;
import cz.jirimuller.eshop.exception.ItaGenreNotFoundException;
import cz.jirimuller.eshop.mapper.GenreMapper;
import cz.jirimuller.eshop.model.GenreDto;
import cz.jirimuller.eshop.repository.GenreRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GenreServiceImplTest implements WithAssertions {

    @Mock
    private GenreRepository genreRepository;

    @Mock
    private GenreMapper genreMapper;

    @InjectMocks
    private GenreServiceImpl service;

    @Test
    void getById() {
        // arrange
        final LocalDateTime now = LocalDateTime.now();

        final Genre expectedGenre = (Genre) new Genre()
                .setName("Genre")
                .setId(1L)
                .setCreatedAt(now)
                .setModifiedAt(now);

        final GenreDto expectedResult = new GenreDto();

        when(genreRepository.findById(1L))
                .thenReturn(Optional.of(expectedGenre));
        when(genreMapper.mapToDto(expectedGenre))
                .thenReturn(expectedResult);

        // act
        final GenreDto byId = service.getById(1L);

        // assert
        assertThat(byId).isNotNull();
        assertThat(byId).isEqualTo(expectedResult);
        verify(genreRepository).findById(1L);
        verify(genreMapper).mapToDto(expectedGenre);
        verifyNoMoreInteractions(genreMapper, genreRepository);
    }

    @Test
    void getById_genreNotFound() {
        // arrange
        when(genreRepository.findById(1L))
                .thenReturn(Optional.empty());

        // act & assert
        final ItaGenreNotFoundException expectedException = assertThrows(ItaGenreNotFoundException.class, () -> service.getById(1L));
        assertThat(expectedException.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(genreRepository).findById(1L);
    }

    @Test
    void getAll() {
        // arrange
        final Genre genre0 = new Genre();
        final Genre genre1 = new Genre();
        final Genre genre2 = new Genre();
        final List<Genre> listAll = List.of(genre0, genre1, genre2);
        final List<GenreDto> listAllDto = listAll.stream().map(genreMapper::mapToDto).collect(Collectors.toList());

        when(genreRepository.findAll())
                .thenReturn(listAll);

        // act
        final List<GenreDto> all = service.getAll();

        // assert
        assertThat(all).containsExactlyInAnyOrderElementsOf(listAllDto);
        verify(genreRepository).findAll();
        verify(genreMapper, times(6)).mapToDto(any());
        verifyNoMoreInteractions(genreMapper, genreRepository);
    }

    @Test
    void getAll_noData() {
        // arrange
        final List<Genre> emptyList = new LinkedList<>();

        when(genreRepository.findAll())
                .thenReturn(emptyList);

        // act
        final List<GenreDto> emptyResult = service.getAll();

        // assert
        assertThat(emptyResult).isEqualTo(emptyList);
        verify(genreRepository).findAll();
        verifyNoMoreInteractions(genreMapper, genreRepository);
    }
}
