package cz.jirimuller.eshop.service.impl;

import cz.jirimuller.eshop.domain.Author;
import cz.jirimuller.eshop.exception.ItaAuthorNotFoundException;
import cz.jirimuller.eshop.mapper.AuthorMapper;
import cz.jirimuller.eshop.model.AuthorDto;
import cz.jirimuller.eshop.repository.AuthorRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AuthorServiceImplTest implements WithAssertions {

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private AuthorMapper authorMapper;

    @InjectMocks
    private AuthorServiceImpl service;

    @Test
    void getById() {
        // arrange
        final LocalDateTime now = LocalDateTime.now();

        final Author expectedAuthor = (Author) new Author()
                .setName("Name")
                .setBio("Desc")
                .setId(1L)
                .setCreatedAt(now)
                .setModifiedAt(now);

        final AuthorDto expectedResult = new AuthorDto();

        when(authorRepository.findById(1L))
                .thenReturn(Optional.of(expectedAuthor));
        when(authorMapper.mapToDto(expectedAuthor))
                .thenReturn(expectedResult);

        // act
        final AuthorDto byId = service.getById(1L);

        // assert
        assertThat(byId).isNotNull();
        assertThat(byId).isEqualTo(expectedResult);
        verify(authorRepository).findById(1L);
        verify(authorMapper).mapToDto(expectedAuthor);
        verifyNoMoreInteractions(authorMapper, authorRepository);
    }

    @Test
    void getById_authorNotFound() {
        // arrange
        when(authorRepository.findById(1L))
                .thenReturn(Optional.empty());

        // act & assert
        final ItaAuthorNotFoundException expectedException = assertThrows(ItaAuthorNotFoundException.class, () -> service.getById(1L));
        assertThat(expectedException.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(authorRepository).findById(1L);
    }

    @Test
    void getAll() {
        // arrange
        final Author author0 = new Author();
        final Author author1 = new Author();
        final Author author2 = new Author();
        final List<Author> listAll = List.of(author0, author1, author2);
        final List<AuthorDto> listAllDto = listAll.stream().map(authorMapper::mapToDto).collect(Collectors.toList());

        when(authorRepository.findAll())
                .thenReturn(listAll);

        // act
        final List<AuthorDto> all = service.getAll();

        // assert
        assertThat(all).containsExactlyInAnyOrderElementsOf(listAllDto);
        verify(authorRepository).findAll();
        verify(authorMapper, times(6)).mapToDto(any());
        verifyNoMoreInteractions(authorMapper, authorRepository);
    }

    @Test
    void getAll_noData() {
        // arrange
        final List<Author> emptyList = new LinkedList<>();

        when(authorRepository.findAll())
                .thenReturn(emptyList);

        // act
        final List<AuthorDto> emptyResult = service.getAll();

        // assert
        assertThat(emptyResult).isEqualTo(emptyList);
        verify(authorRepository).findAll();
        verifyNoMoreInteractions(authorMapper, authorRepository);
    }
}
