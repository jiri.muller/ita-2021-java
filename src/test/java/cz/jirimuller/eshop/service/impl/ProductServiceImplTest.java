package cz.jirimuller.eshop.service.impl;

import cz.jirimuller.eshop.domain.Product;
import cz.jirimuller.eshop.exception.ItaProductNotFoundException;
import cz.jirimuller.eshop.mapper.ProductMapper;
import cz.jirimuller.eshop.model.ProductDto;
import cz.jirimuller.eshop.model.ProductRequestDto;
import cz.jirimuller.eshop.repository.ProductRepository;

import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest implements WithAssertions {

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductMapper productMapper;

    @InjectMocks
    private ProductServiceImpl service;

    @Test
    void getById() {
        // arrange
        final Product expectedProduct = (Product) new Product()
                .setName("Name")
                .setDescription("Desc")
                .setImage("image.jpg")
                .setPrice(BigDecimal.TEN)
                .setStock(10L)
                .setId(1L);

        final ProductDto expectedResult = new ProductDto();

        when(productRepository.findById(1L))
                .thenReturn(Optional.of(expectedProduct));
        when(productMapper.mapToDto(expectedProduct))
                .thenReturn(expectedResult);

        // act
        final ProductDto byId = service.getById(1L);

        // assert
        assertThat(byId).isNotNull();
        assertThat(byId).isEqualTo(expectedResult);
        verify(productRepository).findById(1L);
        verify(productMapper).mapToDto(expectedProduct);
        verifyNoMoreInteractions(productMapper, productRepository);
    }

    @Test
    void getById_productNotFound() {
        // arrange
        when(productRepository.findById(1L))
                .thenReturn(Optional.empty());

        // act & assert
        final ItaProductNotFoundException expectedException = assertThrows(ItaProductNotFoundException.class, () -> service.getById(1L));
        assertThat(expectedException.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(productRepository).findById(1L);
        verifyNoMoreInteractions(productMapper, productRepository);
    }

    @Test
    void getAll() {
        // arrange
        final Product product0 = new Product();
        final Product product1 = new Product();
        final Product product2 = new Product();
        final List<Product> listAll = List.of(product0, product1, product2);
        final List<ProductDto> listAllDto = listAll.stream().map(productMapper::mapToDto).collect(Collectors.toList());

        when(productRepository.findAll())
                .thenReturn(listAll);

        // act
        final List<ProductDto> all = service.getAll();

        // assert
        assertThat(all).containsExactlyInAnyOrderElementsOf(listAllDto);
        verify(productRepository).findAll();
        verify(productMapper, times(6)).mapToDto(any());
        verifyNoMoreInteractions(productMapper, productRepository);
    }

    @Test
    void getAll_noData() {
        // arrange
        final List<Product> emptyList = new LinkedList<>();

        when(productRepository.findAll())
                .thenReturn(emptyList);

        // act
        final List<ProductDto> emptyResult = service.getAll();

        // assert
        assertThat(emptyResult).isEqualTo(emptyList);
        verify(productRepository).findAll();
        verifyNoMoreInteractions(productMapper, productRepository);
    }

    @Test
    void saveProduct() {
        // arrange
        final ProductRequestDto request = new ProductRequestDto()
                .setName("Something")
                .setDescription("This is something")
                .setImage("img.jpg")
                .setPrice(BigDecimal.TEN)
                .setStock(22L);

        final Product product = new Product();
        final ProductDto productDto = new ProductDto();

        when(productRepository.save(product))
                .thenReturn(product);
        when(productMapper.mapToEntity(request))
                .thenReturn(product);
        when(productMapper.mapToDto(product))
                .thenReturn(productDto);

        // act
        ProductDto result = service.createProduct(request);

        // assert
        assertThat(result).isNotNull();
        verify(productRepository).save(product);
        verify(productMapper).mapToDto(product);
        verify(productMapper).mapToEntity(request);
        verifyNoMoreInteractions(productMapper, productRepository);
    }

    @Test
    void updateProduct() {
        // arrange
        ProductRequestDto request = new ProductRequestDto();
        Product product = new Product();
        ProductDto response = new ProductDto();

        when(productMapper.mapToEntity(request))
                .thenReturn(product);
        when(productMapper.mapToDto(product))
                .thenReturn(response);
        when(productRepository.existsById(anyLong()))
                .thenReturn(Boolean.TRUE);
        when(productRepository.save(product))
                .thenReturn(product);

        // act
        final ProductDto result = service.updateProduct(1L, request);

        // assert
        assertThat(result).isNotNull();
        verify(productMapper).mapToDto(product);
        verify(productMapper).mapToEntity(request);
        verify(productRepository).findById(1L);
        verify(productRepository).save(product);
        verifyNoMoreInteractions(productMapper, productRepository);
    }

    @Test
    void updateProduct_productNotFound() {
        // arrange
        when(productRepository.existsById(1L))
                .thenReturn(Boolean.FALSE);
        ProductRequestDto request = new ProductRequestDto();

        // act & assert
        final ItaProductNotFoundException expectedException = assertThrows(ItaProductNotFoundException.class,
                () -> service.updateProduct(1L, request));
        assertThat(expectedException.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(productRepository).existsById(1L);
        verifyNoMoreInteractions(productMapper, productRepository);
    }

    @Test
    void deleteProduct() {
        // arrange
        Product product = new Product();
        when(productRepository.findById(1L))
                .thenReturn(Optional.of(product));
        doNothing().when(productRepository).delete(product);

        // act
        service.deleteProduct(1L);

        // assert
        verify(productRepository).delete(product);
        verifyNoMoreInteractions(productMapper, productRepository);
    }

    @Test
    void deleteProduct_productNotFound() {
        // act & assert
        final ItaProductNotFoundException expectedException = assertThrows(ItaProductNotFoundException.class,
                () -> service.deleteProduct(1L));
        assertThat(expectedException.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(productRepository).findById(1L);
        verifyNoMoreInteractions(productMapper, productRepository);
    }
}