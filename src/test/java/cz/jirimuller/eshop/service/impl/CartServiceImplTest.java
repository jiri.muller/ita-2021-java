package cz.jirimuller.eshop.service.impl;

import cz.jirimuller.eshop.domain.Cart;
import cz.jirimuller.eshop.exception.ItaCartNotFoundException;
import cz.jirimuller.eshop.mapper.CartMapper;
import cz.jirimuller.eshop.model.CartDto;
import cz.jirimuller.eshop.repository.CartRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CartServiceImplTest implements WithAssertions {

    @Mock
    private CartRepository cartRepository;

    @Mock
    private CartMapper cartMapper;

    @InjectMocks
    private CartServiceImpl service;

    @Test
    void getById() {
        // arrange
        final Cart expectedCart = (Cart) new Cart()
                .setId(1L);

        final CartDto expectedResult = new CartDto();

        when(cartRepository.findById(1L))
                .thenReturn(Optional.of(expectedCart));
        when(cartMapper.mapToDto(expectedCart))
                .thenReturn(expectedResult);

        // act
        final CartDto byId = service.getById(1L);

        // assert
        assertThat(byId).isNotNull();
        assertThat(byId).isEqualTo(expectedResult);
        verify(cartRepository).findById(1L);
        verify(cartMapper).mapToDto(expectedCart);
        verifyNoMoreInteractions(cartMapper, cartRepository);
    }

    @Test
    void getById_cartNotFound() {
        // arrange
        when(cartRepository.findById(1L))
                .thenReturn(Optional.empty());

        // act & assert
        final ItaCartNotFoundException expectedException = assertThrows(ItaCartNotFoundException.class, () -> service.getById(1L));
        assertThat(expectedException.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        verify(cartRepository).findById(1L);
        verifyNoMoreInteractions(cartMapper, cartRepository);
    }
}
