package cz.jirimuller.eshop.validation;


import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class StartsWithUppercaseValidatorTest implements WithAssertions  {

    private final StartsWithUppercaseValidator validator = new StartsWithUppercaseValidator();

    private static Stream<Arguments> isValid() {
        // arrange
        return Stream.of(
                Arguments.of("", true),
                Arguments.of("P", true),
                Arguments.of("LongerThingy", true),
                Arguments.of(" ", false),
                Arguments.of("l", false),
                Arguments.of(" P", false),
                Arguments.of(" l", false)
        );
    }

    @ParameterizedTest
    @MethodSource
    void isValid(String testString, boolean expected) {
        // act
        final boolean result = validator.isValid(testString, null);

        // assert
        assertThat(result);
    }
}
