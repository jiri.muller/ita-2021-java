package cz.jirimuller.eshop.domain;

public enum OrderStatus {
    NEW, COMPLETED, CANCELLED
}
