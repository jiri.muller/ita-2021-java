package cz.jirimuller.eshop.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
@Data
public class Product extends BaseEntity {
    private String name;
    @Size(max = 2048)
    private String description;
    private BigDecimal price;
    private Long stock;
    private String image;

    @ManyToOne
    @JoinColumn(name = "id_author")
    private Author author;

    @ManyToOne
    @JoinColumn(name = "id_genre")
    private Genre genre;
}
