package cz.jirimuller.eshop.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.validation.constraints.Size;

@Entity
@Data
public class Author extends BaseEntity {
    private String name;
    @Size(max = 1024)
    private String bio;
}
