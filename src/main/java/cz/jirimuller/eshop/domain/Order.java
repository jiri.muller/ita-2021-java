package cz.jirimuller.eshop.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "order_t")
public class Order extends BaseEntity {

    private String name;
    private String address;
    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @ManyToMany
    @JoinColumn(name = "id")
    private List<Product> products;
}
