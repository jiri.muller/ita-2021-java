package cz.jirimuller.eshop.domain;


import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class Genre extends BaseEntity {
    private String name;
    private String description;
}
