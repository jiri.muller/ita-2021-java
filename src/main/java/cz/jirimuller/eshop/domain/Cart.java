package cz.jirimuller.eshop.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
@Data
public class Cart extends BaseEntity {
    @ManyToMany
    @JoinTable(
            name = "r_cart_product",
            joinColumns = {@JoinColumn(name = "id_cart")},
            inverseJoinColumns = {@JoinColumn(name = "id_product")}
    )
    private List<Product> products;
}
