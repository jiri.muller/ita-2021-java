package cz.jirimuller.eshop.repository;

import cz.jirimuller.eshop.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
