package cz.jirimuller.eshop.repository;

import cz.jirimuller.eshop.domain.Genre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenreRepository extends JpaRepository<Genre, Long> {
}
