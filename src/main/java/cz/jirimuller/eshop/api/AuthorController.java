package cz.jirimuller.eshop.api;

import cz.jirimuller.eshop.model.AuthorDto;
import cz.jirimuller.eshop.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("https://master.d1z6pirlbp1239.amplifyapp.com")
@RestController
@RequestMapping("/api/v1/authors")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService service;

    @GetMapping("{id}")
    public AuthorDto getById(@PathVariable(name = "id") Long id) {
        return service.getById(id);
    }

    @GetMapping
    public List<AuthorDto> getAll () {
        return service.getAll();
    }

}
