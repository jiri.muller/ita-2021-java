package cz.jirimuller.eshop.api;

import cz.jirimuller.eshop.model.ProductDto;
import cz.jirimuller.eshop.model.ProductRequestDto;
import cz.jirimuller.eshop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin("https://master.d1z6pirlbp1239.amplifyapp.com")
@RestController
@RequestMapping("/api/v1/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService service;

    @GetMapping("{id}")
    public ProductDto getProduct(@PathVariable Long id) {
        return service.getById(id);
    }

    @GetMapping
    public List<ProductDto> getAllProducts () {
        return service.getAll();
    }

    @PostMapping()
    public ProductDto createProduct(@Valid @RequestBody ProductRequestDto request) {
        return service.createProduct(request);
    }

    @PutMapping("{id}")
    public ProductDto updateProduct(@Valid @PathVariable Long id, @RequestBody ProductRequestDto request) {
        return service.updateProduct(id, request);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProduct(@PathVariable Long id) {
        service.deleteProduct(id);
    }
}
