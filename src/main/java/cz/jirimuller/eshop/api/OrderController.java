package cz.jirimuller.eshop.api;


import cz.jirimuller.eshop.model.OrderDto;
import cz.jirimuller.eshop.model.OrderRequestDto;
import cz.jirimuller.eshop.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin("https://master.d1z6pirlbp1239.amplifyapp.com")
@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService service;

    @PostMapping
    private OrderDto createOrder(@Valid @RequestBody OrderRequestDto request) {
        return service.createOrder(request);
    }
}
