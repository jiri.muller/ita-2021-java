package cz.jirimuller.eshop.api;

import cz.jirimuller.eshop.model.CartDto;
import cz.jirimuller.eshop.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("https://master.d1z6pirlbp1239.amplifyapp.com")
@RestController
@RequestMapping("/api/v1/carts")
@RequiredArgsConstructor
public class CartController {

    private final CartService service;

    @GetMapping("{id}")
    public CartDto getById(@PathVariable(name = "id") Long id) {
        return service.getById(id);
    }

    @PostMapping("products/{id}")
    public CartDto createCartWithProduct(@PathVariable(name = "id") Long id) {
        return service.createCartWithProduct(id);
    }

    @PutMapping("{cartId}/products/{productId}")
    public CartDto loadProductToCart(@PathVariable(name = "cartId") Long cartId,
                                     @PathVariable(name = "productId") Long productId) {
        return service.loadProductToCart(cartId, productId);
    }
}