package cz.jirimuller.eshop.service;

import cz.jirimuller.eshop.model.AuthorDto;

import java.util.List;

public interface AuthorService {
    AuthorDto getById(Long id);
    List<AuthorDto> getAll ();
}
