package cz.jirimuller.eshop.service;

import cz.jirimuller.eshop.model.OrderDto;
import cz.jirimuller.eshop.model.OrderRequestDto;

public interface OrderService {
    OrderDto createOrder(OrderRequestDto request);
}
