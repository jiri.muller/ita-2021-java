package cz.jirimuller.eshop.service.impl;

import cz.jirimuller.eshop.domain.Author;
import cz.jirimuller.eshop.exception.ItaAuthorNotFoundException;
import cz.jirimuller.eshop.mapper.AuthorMapper;
import cz.jirimuller.eshop.model.AuthorDto;
import cz.jirimuller.eshop.repository.AuthorRepository;
import cz.jirimuller.eshop.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Service
@Transactional
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private static final Logger log = LoggerFactory.getLogger(AuthorServiceImpl.class);
    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;

    @Transactional(readOnly = true)
    @Override
    public AuthorDto getById(Long id) {

        log.info("Getting author by id");
        log.debug("getById (author) was called with id {}", id);

        final Author author = authorRepository.findById(id)
                .orElseThrow(() -> new ItaAuthorNotFoundException(id));
        final AuthorDto authorDto = authorMapper.mapToDto(author);

        log.debug("getById (author) returned {}", authorDto.toString());

        return authorDto;
    }

    @Transactional(readOnly = true)
    @Override
    public List<AuthorDto> getAll() {

        log.info("Getting all authors");

        final List<AuthorDto> all = authorRepository.findAll().stream()
                .map(authorMapper::mapToDto)
                .collect(Collectors.toList());

        log.debug("getAll (author) returned {}", all);

        return all;
    }
}
