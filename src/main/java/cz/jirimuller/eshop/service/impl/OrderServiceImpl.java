package cz.jirimuller.eshop.service.impl;

import cz.jirimuller.eshop.domain.Cart;
import cz.jirimuller.eshop.domain.Order;
import cz.jirimuller.eshop.domain.OrderStatus;
import cz.jirimuller.eshop.domain.Product;
import cz.jirimuller.eshop.exception.ItaCartNotFoundException;
import cz.jirimuller.eshop.mapper.OrderMapper;
import cz.jirimuller.eshop.model.OrderDto;
import cz.jirimuller.eshop.model.OrderRequestDto;
import cz.jirimuller.eshop.repository.CartRepository;
import cz.jirimuller.eshop.repository.OrderRepository;
import cz.jirimuller.eshop.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private static final Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);
    final CartRepository cartRepository;
    final OrderRepository orderRepository;
    final OrderMapper orderMapper;

    @Override
    public OrderDto createOrder(OrderRequestDto request) {

        log.info("Creating a new order");
        log.debug("createOrder was called with request {}", request.toString());

        final Long idCart = request.getIdCart();
        final Cart cart = cartRepository.findById(idCart)
                .orElseThrow(() -> new ItaCartNotFoundException(idCart));

        final List<Product> products = List.copyOf(cart.getProducts());
        final Order newOrder = orderMapper.mapToEntity(request)
                .setProducts(products)
                .setStatus(OrderStatus.NEW);

        final Order savedOrder = orderRepository.save(newOrder);
        final OrderDto response = orderMapper.mapToDto(savedOrder);

        log.debug("createOrder returned {}", response.toString());

        return response;
    }
}
