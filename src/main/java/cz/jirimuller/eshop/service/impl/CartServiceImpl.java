package cz.jirimuller.eshop.service.impl;

import cz.jirimuller.eshop.domain.Cart;
import cz.jirimuller.eshop.domain.Product;
import cz.jirimuller.eshop.exception.ItaCartNotFoundException;
import cz.jirimuller.eshop.exception.ItaProductNotFoundException;
import cz.jirimuller.eshop.exception.ItaProductNotInStockException;
import cz.jirimuller.eshop.mapper.CartMapper;
import cz.jirimuller.eshop.model.CartDto;
import cz.jirimuller.eshop.repository.CartRepository;
import cz.jirimuller.eshop.repository.ProductRepository;
import cz.jirimuller.eshop.service.CartService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {

    private static final Logger log = LoggerFactory.getLogger(CartServiceImpl.class);
    private final ProductRepository productRepository;
    private final CartRepository cartRepository;
    private final CartMapper cartMapper;

    @Override
    @Transactional(readOnly = true)
    public CartDto getById(Long id) {

        log.info("Getting cart by id");
        log.debug("getById (cart) was called with id {}", id);

        final Cart cart = cartRepository.findById(id)
                .orElseThrow(() -> new ItaCartNotFoundException(id));

        final CartDto cartDto = cartMapper.mapToDto(cart);

        log.debug("getById (cart) returned {}", cartDto.toString());

        return cartDto;
    }

    @Override
    public CartDto createCartWithProduct(Long id) {

        log.info("Creating a new cart with a product");
        log.debug("createCartWithProduct was called with product id {}", id);

        final Product product = productRepository.findById(id)
                .orElseThrow(() -> new ItaProductNotFoundException(id));

//        if (product.getStock() < 1) {
//            throw new ItaProductNotInStockException(id);
//        }
//        product.setStock(product.getStock - 1L);

        final Cart newCart = new Cart().setProducts(List.of(product));
        final Cart savedCart = cartRepository.save(newCart);
        final CartDto cartDto = cartMapper.mapToDto(savedCart);

        log.debug("createCartWithProduct returned {}", cartDto.toString());

        return cartDto;
    }

    @Override
    public CartDto loadProductToCart(Long cartId, Long productId) {

        log.info("Loading a product into an existing cart");
        log.debug("loadProductToCart was called with product id {} and cart id {}", productId, cartId);

        final Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new ItaCartNotFoundException(cartId));
        final Product productToAdd = productRepository.findById(productId)
                .orElseThrow(() -> new ItaProductNotFoundException(productId));

        List<Product> cartContents = cart.getProducts();
        cartContents.add(productToAdd);
        cart.setProducts(cartContents);

        final CartDto cartDto = cartMapper.mapToDto(cart);

        log.debug("loadProductToCart returned {}", cartDto.toString());

        return cartDto;
    }
}
