package cz.jirimuller.eshop.service.impl;

import cz.jirimuller.eshop.domain.Genre;
import cz.jirimuller.eshop.exception.ItaGenreNotFoundException;
import cz.jirimuller.eshop.mapper.GenreMapper;
import cz.jirimuller.eshop.model.GenreDto;
import cz.jirimuller.eshop.repository.GenreRepository;
import cz.jirimuller.eshop.service.GenreService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class GenreServiceImpl implements GenreService {

    private static final Logger log = LoggerFactory.getLogger(GenreServiceImpl.class);
    private final GenreRepository repository;
    private final GenreMapper mapper;

    @Transactional(readOnly = true)
    @Override
    public GenreDto getById(Long id) {

        log.info("Getting genre by id");
        log.debug("getById (genre) was called with id {}", id);

        final Genre genre = repository.findById(id)
                .orElseThrow(() -> new ItaGenreNotFoundException(id));
        final GenreDto genreDto = mapper.mapToDto(genre);

        log.debug("getById (genre) returned {}", genreDto.toString());

        return genreDto;
    }

    @Transactional(readOnly = true)
    @Override
    public List<GenreDto> getAll() {

        log.info("Getting all genres");

        final List<GenreDto> all = repository.findAll().stream()
                .map(mapper::mapToDto)
                .collect(Collectors.toList());

        log.debug("getAll (genre) returned {}", all);

        return all;
    }
}