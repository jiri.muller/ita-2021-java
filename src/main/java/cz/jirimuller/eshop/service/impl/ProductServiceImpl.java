package cz.jirimuller.eshop.service.impl;

import cz.jirimuller.eshop.domain.Author;
import cz.jirimuller.eshop.domain.Genre;
import cz.jirimuller.eshop.domain.Product;
import cz.jirimuller.eshop.exception.ItaAuthorNotFoundException;
import cz.jirimuller.eshop.exception.ItaGenreNotFoundException;
import cz.jirimuller.eshop.exception.ItaProductNotFoundException;
import cz.jirimuller.eshop.mapper.ProductMapper;
import cz.jirimuller.eshop.model.ProductDto;
import cz.jirimuller.eshop.model.ProductRequestDto;
import cz.jirimuller.eshop.repository.AuthorRepository;
import cz.jirimuller.eshop.repository.GenreRepository;
import cz.jirimuller.eshop.repository.ProductRepository;
import cz.jirimuller.eshop.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Service
@Transactional
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);
    private final ProductRepository productRepository;
    private final AuthorRepository authorRepository;
    private final GenreRepository genreRepository;
    private final ProductMapper mapper;

    @Transactional(readOnly = true)
    @Override
    public ProductDto getById(Long id) {

        log.info("Getting product by id");
        log.debug("getById (product) was called with id {}", id);

        final Product product = productRepository.findById(id)
                .orElseThrow(() -> new ItaProductNotFoundException(id));
        final ProductDto productDto = mapper.mapToDto(product);

        log.debug("getById (product) returned {}", productDto.toString());

        return productDto;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ProductDto> getAll() {

        log.info("Getting all products");

        final List<ProductDto> all = productRepository.findAll().stream()
                .map(mapper::mapToDto)
                .collect(Collectors.toList());

        log.debug("getAll (product) returned {}", all);

        return all;
    }

    @Override
    public ProductDto createProduct(ProductRequestDto request) {

        log.info("Creating product");
        log.debug("createProduct was called with request {}", request.toString());

        final Product product = mapper.mapToEntity(request);

        final Long authorId = request.getAuthorId();
        if (authorId != null) {
            final Author author = authorRepository.findById(authorId)
                    .orElseThrow(() -> new ItaAuthorNotFoundException(authorId));
            product.setAuthor(author);
        }

        final Long genreId = request.getGenreId();
        if (genreId != null) {
            final Genre genre = genreRepository.findById(genreId)
                    .orElseThrow(() -> new ItaGenreNotFoundException(genreId));
            product.setGenre(genre);
        }

        final Product savedProduct = productRepository.save(product);
        final ProductDto productDto = mapper.mapToDto(savedProduct);

        log.info("Product was created");
        log.debug("saveProduct returned {}", productDto);

        return productDto;
    }

    @Override
    public ProductDto updateProduct(Long id, ProductRequestDto request) {

        log.info("Updating product");
        log.debug("updateProduct was called with {} and request {}", id, request.toString());

        final Product product = productRepository.findById(id)
                .orElseThrow(() -> new ItaProductNotFoundException(id));

        mapper.mapToExistingEntity(product, request);

        final ProductDto response = mapper.mapToDto(product);

        log.info("Product was updated");
        log.debug("updateProduct returned {}", response);

        return response;
    }

    @Override
    public void deleteProduct(Long id) {

        log.info("Deleting product");
        log.debug("deleteProduct was called with {} ", id);

        final Product productToDelete = productRepository.findById(id)
                .orElseThrow(() -> new ItaProductNotFoundException(id));

        log.info("Product was deleted");
        log.debug("Product {} was deleted", id);

        productRepository.delete(productToDelete);
    }
}
