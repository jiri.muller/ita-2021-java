package cz.jirimuller.eshop.service;

import cz.jirimuller.eshop.model.CartDto;

public interface CartService {
    CartDto getById(Long id);
    CartDto createCartWithProduct(Long id);
    CartDto loadProductToCart(Long cartId, Long productId);

}
