package cz.jirimuller.eshop.service;

import cz.jirimuller.eshop.model.GenreDto;

import java.util.List;

public interface GenreService {
    GenreDto getById(Long id);
    List<GenreDto> getAll ();
}
