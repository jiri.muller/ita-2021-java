package cz.jirimuller.eshop.service;

import cz.jirimuller.eshop.model.ProductDto;
import cz.jirimuller.eshop.model.ProductRequestDto;

import java.util.List;

public interface ProductService {
    ProductDto getById(Long id);

    List<ProductDto> getAll();

    ProductDto createProduct(ProductRequestDto request);

    ProductDto updateProduct(Long id, ProductRequestDto request);

    void deleteProduct(Long id);
}
