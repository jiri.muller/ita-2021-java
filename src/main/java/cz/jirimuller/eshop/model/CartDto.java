package cz.jirimuller.eshop.model;

import cz.jirimuller.eshop.domain.Product;
import lombok.Data;

import java.util.List;

@Data
public class CartDto {
    private Long id;
    private List<Product> products;
}
