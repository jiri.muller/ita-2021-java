package cz.jirimuller.eshop.model;

import cz.jirimuller.eshop.domain.Author;
import cz.jirimuller.eshop.domain.Genre;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductDto {
    private Long id;
    private String name;
    private String description;
    private BigDecimal price;
    private Long stock;
    private String image;
    private Author author;
    private Genre genre;
}
