package cz.jirimuller.eshop.model;

import cz.jirimuller.eshop.domain.OrderStatus;
import cz.jirimuller.eshop.domain.Product;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;

@Data
public class OrderDto {
    private Long id;
    private String name;
    private String address;
    @Enumerated(EnumType.STRING)
    private OrderStatus status;
    private List<Product> products;
}
