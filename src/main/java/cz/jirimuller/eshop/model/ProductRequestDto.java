package cz.jirimuller.eshop.model;

import cz.jirimuller.eshop.validation.StartsWithUppercase;
import lombok.Data;

import javax.validation.constraints.*;
import java.math.BigDecimal;

@Data
public class ProductRequestDto {
    @NotBlank(message = "product name must not be blank")
    @StartsWithUppercase(message = "product name must start with an uppercase letter")
    @Size(max = 255, message = "product name can be at most 255 characters long")
    private String name;
    @NotBlank(message = "product description must not be blank")
    @StartsWithUppercase(message = "product description must start with an uppercase letter")
    @Size(max = 2048, message = "product description can be at most 2048 characters long")
    private String description;
    @Positive(message = "product price must be greater than zero")
    private BigDecimal price;
    @Min(value = 0, message = "product stock must be non-negative")
    private Long stock;
    @NotBlank(message = "product image must not be blank")
    private String image;
    private Long authorId;
    private Long genreId;
}
