package cz.jirimuller.eshop.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class OrderRequestDto {
    @NotNull
    Long idCart;
    @NotBlank
    String name;
    @NotBlank
    String address;
}
