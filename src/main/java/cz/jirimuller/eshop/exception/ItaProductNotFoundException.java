package cz.jirimuller.eshop.exception;

import org.springframework.http.HttpStatus;

public class ItaProductNotFoundException extends ItaException{

    public ItaProductNotFoundException(Long id) {
        super("Product " + id + "not found.", HttpStatus.NOT_FOUND);
    }
}
