package cz.jirimuller.eshop.exception;

import org.springframework.http.HttpStatus;

public class ItaAuthorNotFoundException extends ItaException{

    public ItaAuthorNotFoundException(Long id) {
        super("Author " + id + "not found.", HttpStatus.NOT_FOUND);
    }
}
