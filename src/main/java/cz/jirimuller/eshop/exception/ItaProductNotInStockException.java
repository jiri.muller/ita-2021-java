package cz.jirimuller.eshop.exception;

import org.springframework.http.HttpStatus;

public class ItaProductNotInStockException extends ItaException {
    public ItaProductNotInStockException(Long id) {
        super("Product " + id + " is not in stock.", HttpStatus.BAD_REQUEST);
    }
}
