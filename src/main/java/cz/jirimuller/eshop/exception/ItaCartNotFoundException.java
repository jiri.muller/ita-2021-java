package cz.jirimuller.eshop.exception;

import org.springframework.http.HttpStatus;

public class ItaCartNotFoundException extends ItaException{

    public ItaCartNotFoundException(Long id) {
        super("Cart " + id + "not found.", HttpStatus.NOT_FOUND);
    }
}
