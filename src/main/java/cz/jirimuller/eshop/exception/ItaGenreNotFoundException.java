package cz.jirimuller.eshop.exception;

import org.springframework.http.HttpStatus;

public class ItaGenreNotFoundException extends ItaException{

    public ItaGenreNotFoundException(Long id) {
        super("Genre " + id + "not found.", HttpStatus.NOT_FOUND);
    }
}
