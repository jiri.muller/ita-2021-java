package cz.jirimuller.eshop.validation;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class StartsWithUppercaseValidator implements
        ConstraintValidator<StartsWithUppercase, String> {

    @Override
    public boolean isValid(String s,
                           ConstraintValidatorContext context) {
        if (s == null || s.isEmpty() || Character.isUpperCase(s.charAt(0))) {
            return true;
        }
        return false;
    }
}
