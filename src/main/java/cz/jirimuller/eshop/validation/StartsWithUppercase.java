package cz.jirimuller.eshop.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = StartsWithUppercaseValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface StartsWithUppercase {
    String message() default "Field must start with uppercase";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}