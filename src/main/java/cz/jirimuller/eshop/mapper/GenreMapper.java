package cz.jirimuller.eshop.mapper;

import cz.jirimuller.eshop.domain.Genre;
import cz.jirimuller.eshop.model.GenreDto;
import org.mapstruct.Mapper;

@Mapper(uses = {ProductMapper.class})
public interface GenreMapper {
    GenreDto mapToDto(Genre genre);
}
