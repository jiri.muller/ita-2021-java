package cz.jirimuller.eshop.mapper;

import cz.jirimuller.eshop.domain.Cart;
import cz.jirimuller.eshop.model.CartDto;
import org.mapstruct.Mapper;

@Mapper(uses = {ProductMapper.class})
public interface CartMapper {
    CartDto mapToDto(Cart cart);
}
