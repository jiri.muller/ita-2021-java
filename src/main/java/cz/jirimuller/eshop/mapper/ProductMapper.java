package cz.jirimuller.eshop.mapper;

import cz.jirimuller.eshop.domain.Product;
import cz.jirimuller.eshop.model.ProductDto;
import cz.jirimuller.eshop.model.ProductRequestDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(uses = {AuthorMapper.class, GenreMapper.class})
public interface ProductMapper {

    Product mapToEntity(ProductRequestDto request);

    Product mapToExistingEntity(@MappingTarget Product existingProduct, ProductRequestDto request);
    ProductDto mapToDto(Product response);
}