package cz.jirimuller.eshop.mapper;

import cz.jirimuller.eshop.domain.Author;
import cz.jirimuller.eshop.model.AuthorDto;
import org.mapstruct.Mapper;

@Mapper(uses = {ProductMapper.class})
public interface AuthorMapper {
    AuthorDto mapToDto(Author author);
}
