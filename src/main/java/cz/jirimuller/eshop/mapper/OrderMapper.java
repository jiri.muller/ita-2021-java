package cz.jirimuller.eshop.mapper;

import cz.jirimuller.eshop.domain.Order;
import cz.jirimuller.eshop.model.OrderDto;
import cz.jirimuller.eshop.model.OrderRequestDto;
import org.mapstruct.Mapper;

@Mapper
public interface OrderMapper {
    Order mapToEntity(OrderRequestDto request);
    OrderDto mapToDto(Order response);
}
