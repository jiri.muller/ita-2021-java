insert into author values (1, now(), now(), 'Sir Terence David John Pratchett OBE (28 April 1948 – 12 March 2015) was an English humorist, satirist, and author of fantasy novels, especially comical works. He is best known for his Discworld series of 41 novels.', 'Terry Pratchett');
insert into author values (2, now(), now(), 'Karel Čapek (9 January 1890 – 25 December 1938) was a Czech writer, playwright and critic. He has become best known for his science fiction, including his novel War with the Newts (1936) and play R.U.R. (Rossum''s Universal Robots, 1920), which introduced the word robot. He also wrote many politically charged works dealing with the social turmoil of his time. Influenced by American pragmatic liberalism, he campaigned in favour of free expression and strongly opposed the rise of both fascism and communism in Europe.', 'Karel Čapek');
insert into author values (3, now(), now(), 'Franz Kafka (3 July 1883 – 3 June 1924) was a German-speaking Bohemian novelist and short-story writer, widely regarded as one of the major figures of 20th-century literature. His work fuses elements of realism and the fantastic. It typically features isolated protagonists facing bizarre or surrealistic predicaments and incomprehensible socio-bureaucratic powers. It has been interpreted as exploring themes of alienation, existential anxiety, guilt, and absurdity His best known works include "Die Verwandlung" ("The Metamorphosis"), Der Process (The Trial), and Das Schloss (The Castle). The term Kafkaesque has entered English to describe situations like those found in his writing.', 'Franz Kafka');

insert into genre values (4, now(), now(), 'Stories composed in verse or prose, usually for theatrical performance, where conflicts and emotion are expressed through dialogue and action.', 'Drama');
insert into genre values (5, now(), now(), 'Narration demonstrating a useful truth, especially in which animals speak as humans; legendary, supernatural tale.', 'Fable');
insert into genre values (6, now(), now(), 'Story about fairies or other magical creatures, usually for children.', 'Fairy Tale');
insert into genre values (7, now(), now(), 'Fiction with strange or other worldly settings or characters; fiction which invites suspension of reality.', 'Fantasy');
insert into genre values (8, now(), now(), 'Narrative literary works whose content is produced by the imagination and is not necessarily based on fact.', 'Fiction');
insert into genre values (10, now(), now(), 'Full-length novels with plot, subplot(s), theme(s), major and minor characters, in which the narrative is presented in (usually blank) verse form.', 'Fiction in Verse');
insert into genre values (11, now(), now(), 'The songs, stories, myths, and proverbs of a people or "folk" as handed down by word of mouth.', 'Folklore');
insert into genre values (12, now(), now(), 'Story with fictional characters and events in a historical setting.', 'Historical Fiction');
insert into genre values (13, now(), now(), 'Fiction in which events evoke a feeling of dread in both the characters and the reader.', 'Horror');
insert into genre values (14, now(), now(), 'Fiction full of fun, fancy, and excitement, meant to entertain; but can be contained in all genres', 'Humor');
insert into genre values (15, now(), now(), 'Story, sometimes of a national or folk hero, which has a basis in fact but also includes imaginative material.', 'Legend');
insert into genre values (16, now(), now(), 'Fiction dealing with the solution of a crime or the unraveling of secrets.', 'Mystery');
insert into genre values (17, now(), now(), 'Legend or traditional narrative, often based in part on historical events, that reveals human behavior and natural phenomena by its symbolism; often pertaining to the actions of the gods.', 'Mythology');
insert into genre values (18, now(), now(), 'Verse and rhythmic writing with imagery that creates emotional responses.', 'Poetry');
insert into genre values (19, now(), now(), 'Story that can actually happen and is true to life.', 'Realistic Fiction');
insert into genre values (20, now(), now(), 'Story based on impact of actual, imagined, or potential science, usually set in the future or on other planets.', 'Science Fiction');
insert into genre values (21, now(), now(), 'Fiction of such brevity that it supports no subplots.', 'Short Story');
insert into genre values (22, now(), now(), 'Humorous story with blatant exaggerations, swaggering heroes who do the impossible with nonchalance.', 'Tall Tale');
insert into genre values (23, now(), now(), 'Narrative of a person''s life, a true story about a real person.', 'Biography/Autobiography');
insert into genre values (24, now(), now(), 'A short literary composition that reflects the author''s outlook or point.', 'Essay');
insert into genre values (25, now(), now(), 'Factual information presented in a format which tells a story.', 'Narrative Nonfiction');
insert into genre values (26, now(), now(), 'Informational text dealing with an actual, real-life subject.', 'Nonfiction');
insert into genre values (27, now(), now(), 'Public address or discourse.', 'Speech');

insert into product values (28, now(), now(), 'In the beginning there was… a turtle.

Somewhere on the frontier between thought and reality exists the Discworld, a parallel time and place which might sound and smell very much like our own, but which looks completely different.

Particularly as it’s carried through space on the back of a giant turtle.

It plays by different rules. But then, some things are the same everywhere. The Disc’s very existence is about to be threatened by a strange new blight: the world’s first tourist, upon whose survival rests the peace and prosperity of the land.

Unfortunately, the person charged with maintaining that survival in the face of robbers, mercenaries and, well, Death, is a spectacularly inept wizard…

The Discworld novels can be read in any order but The Colour of Magic is the first book in the Wizards series.',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/04/thecolourofmagic-paperback.jpg',
                            'The Colour of Magic', 19.99, 234, 1, 7);

insert into product values (29, now(), now(), '‘What shall we do?’ said Twoflower.

‘Panic?’ said Rincewind hopefully. He always held that panic was the best means of survival.

As it moves towards a seemingly inevitable collision with a malevolent red star, the Discworld could do with a hero.

What it doesn’t need is a singularly inept and cowardly wizard, still recovering from the trauma of falling off the edge of the world, or a well-meaning tourist and his luggage which has a mind (and legs) of its own.

Which is a shame, because that’s all there is…

The Discworld novels can be read in any order but The Light Fantastic is the second book in the Wizards series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/04/thelightfantastic-paperback.jpg',
                            'The Light Fantastic', 19.99, 155, 1, 7);

insert into product values (30, now(), now(), 'Death comes to us all. When he came to Mort, he offered him a job.

Death is the Grim Reaper of the Discworld, a black-robed skeleton carrying a scythe who must collect a minimum number of souls in order to keep the momentum of dying, well… alive. He is also fond of cats and endlessly baffled by humanity. Soon Death is yearning to experience what humanity really has to offer. But to do that, he’ll need to hire some help.

It’s an offer Mort can’t refuse. As Death’s apprentice he’ll have free board, use of the company horse – and being dead isn’t compulsory. It’s a dream job – until Mort falls in love with Death’s daughter, Ysabell, and discovers that your boss can be a killer on your love life…

The Discworld novels can be read in any order but Mort is the first book in the Death series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/04/mort-ebook.jpg',
                            'Mort', 19.99, 666, 1, 7);

insert into product values (31, now(), now(), 'Once there was an eighth son of an eighth son. He was, naturally, a wizard. And there it should have ended. However (for reasons we’d better not go into), he had seven sons of his own.

And then he had an eighth son, a wizard squared, a source of magic. A Sourcerer.

Unseen University, the most magical establishment on the Discworld, has finally got its wish: the emergence of a wizard more powerful than they’ve ever seen. You’d think the smartest men on the Disc would have been a little more careful what they wished for.

As the drastic consequences of sourcery begin to unfold, one wizard holds the solution in his cowardly, incompetent hands. Rincewind must take the University’s most precious artefact, the very embodiment of magic itself, and deliver it halfway across the Disc to safety… if he doesn’t make it, the death of all wizardry is at hand. And the end of the world, depending who you listen to.

The Discworld novels can be read in any order but Sourcery is the third book in the Wizards series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/sourcery-paperback.jpg',
                            'Sourcery', 19.99, 331, 1, 7);

insert into product values (32, now(), now(), '‘Destiny is important, see, but people go wrong when they think it controls them. It’s the other way around.’

Three witches – Granny Weatherwax, Nanny Ogg and Magrat Garlick – have gathered on a lonely heath. A king has been cruelly murdered, his throne usurped by his ambitious cousin. An infant heir and the crown of the kingdom, both missing…

Witches don’t have these kinds of dynastic problems themselves – in fact, they don’t have leaders.

Granny Weatherwax is the most highly regarded of these non-existent leaders. But even she is about to find that meddling in royal politics is a lot more complicated than certain playwrights would have you believe…

The Discworld novels can be read in any order but Wyrd Sisters is the second book in the Witches series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/wyrdsisters-ebook.jpg',
                            'Wyrd Sisters', 19.99, 336, 1, 7);

insert into product values (33, now(), now(), '‘Look after the dead’, said the priests, ‘and the dead will look after you.’

Wise words in all probability, but a tall order when, like Pteppic, you have just become the pharaoh of a small and penniless country rather earlier than expected. It’s hard to fulfil your obligations to the ancestors with a depleted treasury that is unlikely to stretch to the building of a monumental pyramid to honour your dead father.

He’d had the best education money could buy of course, but unfortunately the syllabus at the Assassins’ Guild in Ankh-Morpork did not cover running a kingdom and basic financial acumen…

The Discworld novels can be read in any order but Pyramids is a standalone novel. Winner of the prestigious British Fantasy Award when the book was first published in 1989.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/pyramids-paperback.jpg',
                            'Pyramids', 19.99, 982, 1, 7);

insert into product values (34, now(), now(), '‘Vimes ran a practised eye over the assortment before him. It was the usual Ankh-Morpork mob in times of crisis; half of them were here to complain, a quarter of them were here to watch the other half, and the remainder were here to rob, importune or sell hotdogs to the rest.’

Insurrection is in the air in the city of Ankh-Morpork. The Haves and Have-Nots are about to fall out all over again.

Captain Sam Vimes of the city’s ramshackle Night Watch is used to this. It’s enough to drive a man to drink. Well, to drink more. But this time, something is different – the Have-Nots have found the key to a dormant, lethal weapon that even they don’t fully understand, and they’re about to unleash a campaign of terror on the city.

Time for Captain Vimes to sober up…

The Discworld novels can be read in any order but Guards! Guards! is the first book in the City Watch series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/guardsguards-ebook.jpg',
                            'Guards! Guards!', 19.99, 15, 1, 7);

insert into product values (35, now(), now(), 'Eric calls up a demon to grant him three wishes – but what he gets is the Discworld’s most incompetent wizard…

Eric is the Discworld’s only demonology hacker. The trouble is, he’s not very good at it. All he wants is the usual three wishes: to be immortal, rule the world and have the most beautiful woman fall madly in love with him. The usual stuff.

But what he gets is Rincewind, the Disc’s most incompetent wizard, and Rincewind’s Luggage (the world’s most dangerous travel accessory) into the bargain.

Terry Pratchett’s hilarious take on the Faust legend stars many of the Discworld’s most popular characters in an outrageous adventure that will leave Eric wishing once more – this time, quite fervently, that he’d never been born.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/04/eric-pb-670x1024.jpg',
                            'Eric', 19.99, 125, 1, 7);

insert into product values (36, now(), now(), '‘Holy Wood is a different sort of place. People act differently here. Everywhere else the most important things are gods or money or cattle. Here, the most important thing is to be important.’

Cameras roll, for the first time, on the Discworld when alchemists discover the excitement of the silver screen. But what is the dark secret of Holy Wood hill? As the clichés of Tinsel Town pour into the world, it’s up to the Disc’s first film stars to find out…

Join Victor Tugelbend (‘Can’t sing. Can’t dance. Can handle a sword a little’) and Theda Withel (‘I come from a little town you’ve probably never even heard of’) as they battle the forces of evil and cinema advertising. They’re about to discover that the magic of the silver screen might be powerful enough to put an end to, well, everything.

The Discworld novels can be read in any order but Moving Pictures is a standalone novel.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/movingpictures-paperback.jpg',
                            'Moving Pictures', 19.99, 1285, 1, 7);

insert into product values (37, now(), now(), '‘Death has to happen. That’s what bein’ alive is all about. You’re alive, and then you’re dead. It can’t just stop happening.’

But it can. And it has.

Death is missing – presumed gone.

Which leads to the kind of chaos you always get when an important public service is withdrawn. If Death doesn’t come for you, then what are you supposed to do in the meantime? You can’t have the undead wandering about like lost souls – there’s no telling what might happen!

Particularly when they discover that life really is only for the living…

The Discworld novels can be read in any order but Reaper Man is the second book in the Death series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/reaperman-paperback.jpg',
                            'Reaper Man', 19.99, 1585, 1, 7);

insert into product values (38, now(), now(), 'Fairy godmothers develop a very deep understanding about human nature, which makes the good ones kind and the bad ones powerful.

Inheriting a fairy godmother role seemed an easy job… after all, how difficult could it be to make sure that a servant girl doesn’t marry a prince?

Quite hard, actually, even for the witches Granny Weatherwax, Nanny Ogg and Magrat Garlick. That’s the problem with real life – it tends to get in the way of a good story, and a good story is hard to resist.

Servant girls have to marry the prince, whether they want to or not. You can’t fight a Happy Ending, especially when it comes with glass slippers and a rival Fairy Godmother who has made Destiny an offer it can’t refuse.

The Discworld novels can be read in any order but Witches Abroad is the third book in the Witches series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/witchesabroad-paperback.jpg',
                            'Witches Abroad', 19.99, 7155, 1, 7);

insert into product values (39, now(), now(), '‘Just because you can’t explain it, doesn’t mean it’s a miracle.’

On the Discworld, religion is a controversial business.

Everyone has their own opinion, and indeed their own gods, of every shape and size, and all elbowing for space at the top. In such a competitive environment, shape and size can be pretty crucial to make one’s presence felt.

So it’s certainly not helpful for the Great God Om to find himself in the body of a tortoise, a manifestation far below god-like status in anyone’s book.

In such instances, you need an acolyte, and fast. Brutha, the novice, is the Chosen One – or at least the only one available. He wants peace and justice and brotherly love. He also wants the Inquisition to stop torturing him now, please…

The Discworld novels can be read in any order but Small Gods is a standalone novel.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/smallgods-paperback.jpg',
                            'Small Gods', 19.99, 1655, 1, 7);

insert into product values (40, now(), now(), 'It’s Midsummer Night – no time for dreaming. Because sometimes, when there’s more than one reality at play, too much dreaming can make the walls between them come tumbling down.

Unfortunately there’s usually a damned good reason for there being walls between them in the first place – to keep things out. Things who want to make mischief and play havoc with the natural order.

Granny Weatherwax and her tiny coven of witches are up against real elves. And they’re spectacularly nasty creatures. Even in a world of dwarves, wizards, trolls, Morris dancers – and the odd orang-utan – this is going to cause trouble…

The Discworld novels can be read in any order but Lords and Ladies is the fourth book in the Witches series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/lordsandladies-ebook.jpg',
                            'Lords and Ladies', 19.99, 1655, 1, 7);

insert into product values (41, now(), now(), '‘What’s so hard about pulling a sword out of a stone? The real work’s already been done. You ought to make yourself useful and find the man who put the sword in the stone in the first place.’

The City Watch needs MEN! But what it’s got includes Corporal Carrot (technically a dwarf), Lance-constable Cuddy (really a dwarf), Lance-constable Detritus (a troll), Lance-constable Angua (a woman… most of the time) and Corporal Nobbs (disqualified from the human race for shoving).

And they need all the help they can get, because someone in Ankh-Morpork has been getting dangerous ideas – about crowns and legendary swords, and destiny.

And the problem with destiny is, of course, that she is not always careful where she points her finger. One minute you might be minding your own business on a normal if not spectacular career path, the next you might be in the frame for the big job, like saving the world…

The Discworld novels can be read in any order but Men At Arms is the second book in the City Watch series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/menatarms-paperback.jpg',
                            'Men at Arms', 19.99, 1255, 1, 7);

insert into product values (42, now(), now(), 'Being sixteen is always difficult, even more so when there’s a Death in the family. After all, it’s hard to grow up normally when Grandfather rides a white horse and wields a scythe. Especially if he decides to take a well-earned moment to uncover the meaning of life and discover himself in the process, so that you have to take over the family business, and everyone mistakes you for the Tooth Fairy.

And especially when you have to face the new and addictive music that has entered the Discworld. It’s lawless. It changes people. It’s got a beat and you can dance to it.

It’s called ‘Music with Rocks In’.

And it won’t fade away.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/soulmusic-paperback.jpg',
                            'Soul Music', 19.99, 1545, 1, 7);

insert into product values (43, now(), now(), '‘May you live in interesting times’ is the worst thing one can wish on a citizen of Discworld, especially on the distinctly unmagical Rincewind, who has had far too much perilous excitement in his life and can’t even spell wizard.

So when a request for a Great Wizzard arrives in Ankh-Morpork, via carrier albatross from the faraway Counterweight Continent, it’s the endlessly unlucky Rincewind who’s sent as emissary. The oldest (and most heavily fortified) empire on the Disc is in turmoil, and Chaos is building. And, for some incomprehensible reason, someone believes Rincewind will have a mythic role in the ensuing war and wholesale bloodletting.

There are too many heroes already in the world, but there is only one Rincewind. And he owes it to the world to keep that one alive for as long as possible.

The Discworld novels can be read in any order but Interesting Times is the fifth book in the Wizards series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/interestingtimes-paperback.jpg',
                            'Interesting Times', 19.99, 1525, 1, 7);

insert into product values (44, now(), now(), 'What sort of person sits down and writes a maniacal laugh? And all those exclamation marks, you notice? Five? A sure sign of someone who wears his underpants on his head. Opera can do that to a man…

It can also bring Death. And plenty of it. In unpleasant variations.

This is the Opera House, Ankh-Morpork… a huge, rambling building, where innocent young sopranos are being targeted by a strangely familiar evil mastermind in a mask and evening dress, with a penchant for lurking in shadows and occasional murder.

But Granny Weatherwax, Discworld’s most formidable witch, is in the audience. And she doesn’t hold with that sort of thing. There’s going to be trouble (but nevertheless a good evenin’s entertainment with murders you can really hum…) and the show MUST go on.

The Discworld novels can be read in any order but Maskerade is the fifth book in the Witches series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/maskerade-paperback.jpg',
                            'Maskerade', 19.99, 1155, 1, 7);

insert into product values (45, now(), now(), '‘Sorry?’ said Carrot. ‘If it’s just a thing, how can it commit murder? A sword is a thing,’ – he drew his own sword; it made an almost silken sound – ‘and of course you can’t blame a sword if someone thrust it at you, sir.’

For Commander Vimes, Head of Ankh-Morpork City Watch, life consists of troubling times, linked together by… well, more troubling times.

Right now, it’s the latter. There’s a werewolf with pre-lunar tension in the city, a dwarf with attitude and a golem who’s begun to think for itself… and that’s just the ordinary trouble. The real problem is more puzzling – people are being murdered, but there’s no trace of anything alive having been at the crime scene.

Now Vimes not only has to find out whodunit, but howdunit too. He’s not even sure what they dun. But soon as he knows what the questions are, he’s going to want some answers.

The Discworld novels can be read in any order but Feet of Clay is the third book in the City Watch series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/feetofclay-paperback.jpg',
                            'The Last Continent', 19.99, 1595, 1, 7);

insert into product values (46, now(), now(), '‘THERE HAS TO BE SOMETHING IN THE STOCKING THAT MAKES A NOISE,’ said Death, ‘OTHERWISE, WHAT IS 4:30 A.M. FOR?’

It’s the night before Hogswatch. And it’s too quiet. Superstition makes things work in the Discworld, and undermining it can have consequences. It’s just not right to find Death creeping down chimneys and trying to say ‘Ho Ho Ho…’

It’s the last night of the year, the time is turning, and if Susan, gothic governess and Death’s granddaughter (sort of), doesn’t sort everything out by morning, there won’t be a morning. Ever again…

The Discworld novels can be read in any order but Hogfather is the fourth book in the Death series; a festive feast of darkness (but with jolly robins and tinsel too).
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/hogfather-paperback.jpg',
                            'The Hogfather', 19.99, 15375, 1, 7);

insert into product values (47, now(), now(), '‘Neighbours… hah. People’d live for ages side by side, nodding at one another amicably on their way to work, and then some trivial thing would happen and someone would be having a garden fork removed from their ear.’

When the neighbours in question are the proud empires of Klatch and Ankh-Morpork, those are going to be some pretty large garden tools indeed. Of course, no one would dream of starting a war without a perfectly good reason… such as a ‘strategic’ piece of old rock in the middle of nowhere.

It is, after all, every citizen’s right to bear arms to defend their own. Even if it isn’t technically their own. And even if they don’t have much in the way of actual weaponry. As two armies march, Commander Vimes of the Ankh-Morpork City Watch faces unpleasant foes who are out to get him . . . and that’s just the people on his side. The enemy might be even worse.

The Discworld novels can be read in any order but Jingo is the fourth book in the City Watch series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/04/jingo-paperback.jpg',
                            'Jingo', 19.99, 55, 1, 7);

insert into product values (48, now(), now(), 'This is the Discworld’s last continent. It’s hot. It’s dry . . . very dry. There was this thing once called The Wet, which no one believes in. Practically everything that’s not poisonous is venomous. But it’s the best bloody place in the world, all right?

And it’ll die in a few days. Except . . . who is this hero striding across the red desert? A man in a hat whose luggage follows him on little legs, who’s about to change history.

Yes, all this place has between itself and doom is Rincewind, the inept wizard who can’t even spell wizard. Still… no worries, eh?

The Discworld novels can be read in any order but The Last Continent is the sixth book in the Wizards series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/thelastcontinent-paperback.jpg',
                            'The Last Continent', 19.99, 5, 1, 7);

insert into product values (49, now(), now(), 'In this life there are givers and takers. It’s safe to say that vampires are very much in the latter camp. They don’t have much time for the givers of this world – except perhaps at mealtimes…

Welcome to Lancre, where the newest residents are a thoroughly modern, sophisticated vampire family. They’ve got style and fancy waistcoats. They’re out of the casket and want a bite of the future.

Everyone knows you don’t invite vampires into your house – unless you want permanent guests – nonetheless the King of Lancre has invited them to stay and celebrate the birth of his daughter. Now these vampires have no intention of leaving… ever.

But they haven’t met the neighbours yet.

Between the vampires and their next meal stand the witches of Lancre: Granny Weatherwax, Nanny Ogg, Magrat and young Agnes. But as the residents of Lancre are about to discover, it will take more than garlic and crucifixes to take back their home.

The Discworld novels can be read in any order but Carpe Jugulum is the sixth book in the Witches series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/carpejugulum-paperback.jpg',
                            'Carpe Jugulum', 19.99, 15511, 1, 7);

insert into product values (50, now(), now(), '‘Well, he thought, so this is diplomacy. It’s lying, only for a better class of people.’

They say that diplomacy is a gentle art. That mastering it is a lifetime’s work. But you do need a certain inclination in that direction. It’s not something you can just pick up on the job.

A few days ago Sam Vimes was a copper – an important copper, true – chief of police – but still, at his core, a policeman. Today he is an ambassador – to the mysterious, fat-rich country of Uberwald. Today, Sam Vimes is also a man on the run.

At some point during his ambassadorship, things went very wrong. It’s snowing. It’s freezing. And if Vimes can’t make it through the forest, with only his wits and the gloomy trousers of Uncle Vanya (don’t ask), there’s going to be a terrible war.

There are monsters on his trail. They’re bright. They’re fast. They’re werewolves – and they’re catching up.

The Discworld novels can be read in any order but The Fifth Elephant is the fifth book in the City Watch series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/thefifthelephant-paperback.jpg',
                            'The Fifth Elephant', 19.99, 15, 1, 7);

insert into product values (51, now(), now(), '‘A lie can run round the world before the truth has got its boots on…’

William de Worde is the accidental editor of the Discworld’s first newspaper. Now he must cope with the traditional perils of a journalist’s life – people who want him dead, a recovering vampire with a suicidal fascination for flash photography, some more people who want him dead in a different way and, worst of all, the man who keeps begging him to publish pictures of his humorously shaped potatoes.

William just wants to get at THE TRUTH. Unfortunately, everyone else wants to get at William. And it’s only the third edition…

The Discworld novels can be read in any order but The Truth is a standalone novel.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/thetruth-paperback.jpg',
                            'The Truth', 19.99, 35, 1, 7);

insert into product values (52, now(), now(), 'The Monks of History have the glamorous job of time management in the Discworld. They store it and pump it from the places where it’s wasted (like the underwater – how much time does a codfish need?) to places like cities, where there’s never enough time.

But with the construction of the world’s first truly accurate clock, a race against, well, time, begins for History monk Lu Tze and his suspiciously talented apprentice Lobsang Ludd.

Because a truly accurate clock will stop time.

And when time stands still, everything in human existence stops with it. Then, there really is no future.

The Discworld novels can be read in any order but Thief of Time is the fifth book in the Death series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/thethiefoftime-paperback.jpg',
                            'The Thief of Time', 19.99, 142, 1, 7);

insert into product values (53, now(), now(), 'A short but perfectly formed complete Discworld novel, fully illustrated in lavish colour throughout, The Last Hero is an essential part of any Discworld collection.

It stars the legendary Cohen the Barbarian, a legend in his own lifetime. Cohen can remember when a hero didn’t have to worry about fences and lawyers and civilisation, and when people didn’t tell you off for killing dragons. But he can’t always remember, these days, where he put his teeth…

So now, with his ancient sword and his new walking stick and his old friends – and they’re very old friends – Cohen the Barbarian is going on one final quest. He’s going to climb the highest mountain in the Discworld and meet his gods. The last hero in the world is going to return what the first hero stole. With a vengeance.

That’ll mean the end of the world, if no one stops him in time.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/04/thelasthero-pb.jpg',
                            'The Last Hero', 19.99, 52, 1, 7);

insert into product values (54, now(), now(), 'Rats! They’re everywhere: in the breadbins, dancing across tabletops, stealing pies from under the cooks’ noses. So what does every town need? A good piper to lure them away.

That’s where Maurice comes in. But he’s only a cat (though one that talks), so although he has the ideas, he needs rats and someone to play the pipe. Who better than the kid to play the pipe? And Dangerous Beans. And Peaches. And Hamnpork (who doesn’t really like what’s been happening since The Change; all a rat leader really needs is to be big and stroppy, thinking is just not his thing). And Darktan. And Sardines. And all the others in the Clan.

Then they arrive in Bad Blintz, which is suffering from a plague of rats, and find there are NO rats anywhere (though the two resident rat catchers seem to have plenty of tails to show, at 50 pence per tail).

Someone else has had ideas, and Maurice is not pleased.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/theamazingmaurice-ebook.jpg',
                            'The Amazing Maurice and his Educated Rodents', 19.99, 335, 1, 7);

insert into product values (55, now(), now(), 'For a policeman, there can be few things worse than a serial killer loose in your city. Except, perhaps, a serial killer who targets coppers, and a city on the brink of bloody revolution.

For Commander Sam Vimes, it all feels horribly familiar. Caught on the roof of a very magical building during a storm, he’s found himself back in his own rough, tough past without even the clothes he was standing up in when the lightning struck. Living in the past is hard, especially when your time travel companion is a serial killer who knows where you live. But he must survive, because he has a job to do: track down the murderer and change the outcome of the rebellion.

The problem is: if he wins, he’s got no wife, no child, no future…

The Discworld novels can be read in any order but Night Watch is the sixth book in the City Watch series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/nightwatch-paperback.jpg',
                            'Night Watch', 19.99, 155, 1, 7);

insert into product values (56, now(), now(), 'Armed with only a frying pan and her common sense, young witch-to-be Tiffany Aching must defend her home against the monsters of Fairyland. Luckily she has some very unusual help: the local Nac Mac Feegle – aka the Wee Free Men – a clan of fierce, sheep-stealing, sword-wielding, six-inch-high blue men.

Together they must face headless horsemen, ferocious grimhounds, terrifying dreams come true, and ultimately the sinister Queen of the Elves herself…

The first book in the Tiffany Aching series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/theweefreemen-paperback.jpg',
                            'The Wee Free Men', 19.99, 0, 1, 7);

insert into product values (57, now(), now(), '‘Trousers. That’s the secret… Put on trousers and the world changes. We walk different. We act different. I see these girls and I think: idiots! Get yourself some trousers!’

Women belong in the kitchen –  everyone knows that. Not in jobs, pubs or indeed trousers, and certainly not on the front line.

Nonetheless, Polly Perks has to become a boy in a hurry if she wants to find her missing brother in the army. Cutting off her hair and wearing the trousers is easy. Learning to fart and belch in public and walk like an ape takes more time.

There’s a war on. There’s always a war on, and Polly and her fellow raw recruits are suddenly in the thick of it.

All they have on their side is the most artful sergeant in the army, a vampire with a lust for coffee, and a secret. It’s time to make a stand.

The Discworld novels can be read in any order but Monstrous Regiment is a standalone novel.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/monstrousregiment-paperback.jpg',
                            'Monstrous Regiment', 19.99, 35, 1, 7);

insert into product values (58, now(), now(), 'Tiffany Aching is ready to begin her apprenticeship in magic. She expects spells and magic – not chores and ill-tempered goats! Surely there must be more to witchcraft than this.

What Tiffany doesn’t know is that an insidious, disembodied creature is pursuing her. This time, neither Mistress Weatherwax (the greatest witch in the world) nor the fierce, six-inch-high Wee Free Men can protect her. In the end, it will take all of Tiffany’s inner strength to save herself… if it can be done at all.

The second book in the Tiffany Aching sequence.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/04/ahatfullofsky-paperback.jpg',
                            'A Hat Full of Sky', 19.99, 2225, 1, 7);


insert into product values (59, now(), now(), 'The post was an old thing, of course, but it was so old that it had magically become new again.

Moist von Lipwig is a con artist and a fraud and a man faced with a life choice: be hanged, or put the ailing postal service of Ankh-Morpork – the Discworld’s city-state –  back on its feet.

It’s a tough decision.

The post is a creaking old institution, overshadowed by new technology. But there are people who still believe in it, and Moist must become one of them if he’s going to see that the mail gets through, come rain, hail, sleet, dogs, the Post Office Workers Friendly and Benevolent Society, an evil chairman… and a midnight killer.

Getting a date with Adora Bell Dearheart would be nice, too.

Perhaps there’s a shot at redemption in the mad world of the mail, waiting for a man who’s prepared to push the envelope…

The Discworld novels can be read in any order but Going Postal is the first book in the Moist von Lipwig series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/goingpostal-paperback.jpg',
                            'Going Postal', 19.99, 1599, 1, 7);

insert into product values (60, now(), now(), '‘Beating people up in little rooms… he knew where that led. And if you did it for a good reason, you’d do it for a bad one. You couldn’t say “we’re the good guys” and do bad-guy things.’

Koom Valley, the ancient battle where the trolls ambushed the dwarfs, or the dwarfs ambushed the trolls, was a long time ago.

But if he doesn’t solve the murder of just one dwarf, Commander Sam Vimes of Ankh-Morpork City Watch is going to see it fought again, right outside his office.

With his beloved Watch crumbling around him and the war-drums sounding, Vimes must unravel every clue, outwit every assassin and brave any darkness to find the solution. And the darkness is following him.

Oh… and at six o’clock every day, without fail, with no excuses, he must go home to read Where’s My Cow?, with all the right farmyard noises, to his little boy.

There are some things you have to do.

The Discworld novels can be read in any order but Thud! is the seventh book in the City Watch series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/thud-paperback.jpg',
                            'Thud!', 19.99, 165, 1, 7);

insert into product values (61, now(), now(), '‘Crivens!’

Tiffany Aching put one foot wrong, made just one little mistake . . .

And now the spirit of winter is in love with her. He gives her roses and icebergs and showers her with snowflakes, which is tough when you’re thirteen, but also just a little bit… cool.

And if Tiffany doesn’t work out how to deal with him, there will never be another springtime…

Crackling with energy and humour, Wintersmith is the third tale in a sequence about Tiffany Aching and the Wee Free Men – the Nac Mac Feegles who are determined to help Tiffany, whether she wants it or not.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/04/wintersmith-paperback-1.jpg',
                            'Wintersmith', 19.99, 223, 1, 7);

insert into product values (62, now(), now(), 'Whoever said you can’t fool an honest man wasn’t one.

The Royal Bank is facing a crisis, and it’s time for a change of management.

There are a few problems that may arise with the job… the Chief Cashier is almost certainly a vampire –  there’s something nameless in the cellar and it turns out that the Royal Mint runs at a loss. Meanwhile, people actually want to know where the money’s gone. It’s a job for life. But, as former con-man Moist von Lipwig is learning, that life is not necessarily a long one. He’s about to be exposed as a fraud, but if he’s lucky the Assassins’ Guild might get him first. In fact, a lot of people want him dead. Everywhere he looks he’s making enemies.

Oh. And every day he has to take the Chairman for walkies.

The Discworld novels can be read in any order but Making Money is the second book in the Moist von Lipwig series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/04/makingmoney-paperback.jpg',
                            'Making Money', 19.99, 19, 1, 7);

insert into product values (63, now(), now(), '‘There are all kinds of darkness, and all kinds of things can be found in them, imprisoned, banished, lost or hidden. Sometimes they escape. Sometimes they simply fall out. Sometimes they just can’t take it any more.’

Football has come to the ancient city of Ankh-Morpork. And now, the wizards of Unseen University must win a football match, without using magic, so they’re in the mood for trying everything else.

This is not going to be a gentleman’s game.

The prospect of the Big Match draws in a street urchin with a wonderful talent for kicking a tin can, a maker of jolly good pies, a dim but beautiful young woman, who might just turn out to be the greatest fashion model there has ever been, and the mysterious Mr Nutt. No one knows anything much about Mr Nutt (not even Mr Nutt) but there is something powerful, and dark, locked away inside him.

As the match approaches, secrets are forced into the light and four lives will be entangled and changed for ever. Here we go, here we go, here we go!

The Discworld novels can be read in any order but Unseen Academicals is the seventh book in the Wizards series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/unseen-academicals-paperback.jpg',
                            'Unseen Academicals', 19.99, 1445, 1, 7);

insert into product values (64, now(), now(), 'As the witch of the Chalk, Tiffany Aching performs the distinctly unglamorous work of caring for the needy.

But someone – or something – is inciting fear, generating dark thoughts and angry murmurs against witches.

Tiffany must find the source of unrest and defeat the evil at its root. Aided by the tiny-but-tough Wee Free Men, Tiffany faces a dire challenge, for if she falls, the whole Chalk falls with her…

The fourth book in the Tiffany Aching sequence.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/ishallwearmidnight-paperback.jpg',
                            'I Shall Wear Midnight', 19.99, 16, 1, 7);

insert into product values (65, now(), now(), '‘The jurisdiction of a good man extends to the end of the world.’

It is a truth universally acknowledged that a policeman taking a holiday would barely have had time to open his suitcase before he finds his first corpse.

Commander Sam Vimes of the Ankh-Morpork City Watch is on holiday in the pleasant and innocent countryside, but a body in the wardrobe would be far too simple. Instead he finds many, many bodies –  and an ancient crime more terrible than murder.

He is out of his jurisdiction, out of his depth, out of bacon sandwiches and out of his mind, but never out of guile. Where there is a crime there must be a punishment.

They say that in the end all sins are forgiven. Vimes is about to uncover the exception.

Winner of the Bollinger Everyman Wodehouse Prize for Comic Fiction.

The Discworld novels can be read in any order but Snuff is the eighth book in the City Watch series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/snuff-ebook.jpg',
                            'Snuff', 19.99, 1155, 1, 7);

insert into product values (66, now(), now(), 'Change is in the air for Moist von Lipwig, swindler, con-man, and (naturally) head of the Royal Bank and Post Office.

A steaming, clanging new invention – a steam locomotive named Iron Girder, to be precise –  is drawing astonished crowds. Suddenly it’s a matter of national importance that the trains run on time.

Moist does not enjoy hard work. His input at the bank and post office consists mainly of words, which are not that heavy. Or greasy. And it certainly doesn’t involve rickety bridges, runaway cheeses or a fat controller with knuckledusters.

What Moist does enjoy is being alive, which may not be a perk of running the new railway. Because, of course, some people have OBJECTIONS, and they’ll go to extremes to stop locomotion in its tracks.

The Discworld novels can be read in any order but Raising Steam is the third and final book in the Moist von Lipwig series.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/raisingsteam-paperback.jpg',
                            'Raising Steam', 19.99, 1545, 1, 7);

insert into product values (67, now(), now(), 'Deep in the Chalk, something is stirring. The owls and the foxes can sense it, and Tiffany Aching feels it in her boots. An old enemy is gathering strength.

This is a time of endings and beginnings, old friends and new, a blurring of edges and a shifting of power. Now Tiffany stands between the light and the dark, the good and the bad.

As the fairy horde prepares for invasion, Tiffany must summon all the witches to stand with her. To protect the land. Her land.

There will be a reckoning…

The final Discworld novel.
',
                            'https://www.terrypratchettbooks.com/wp-content/uploads/2019/03/theshepherdscrown-paperback.jpg',
                            'The Shepherd''s Crown', 19.99, 1565, 1, 7);