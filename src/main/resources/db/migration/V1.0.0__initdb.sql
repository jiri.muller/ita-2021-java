create table author
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    bio         varchar(1024),
    name        varchar(255),
    primary key (id)
);
create table cart
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    primary key (id)
);
create table genre
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    description varchar(255),
    name        varchar(255),
    primary key (id)
);
create table order_t
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    address     varchar(255),
    name        varchar(255),
    status      varchar(255),
    primary key (id)
);
create table order_t_products
(
    order_id    int8 not null,
    products_id int8 not null
);
create table product
(
    id          int8 not null,
    created_at  timestamp,
    modified_at timestamp,
    description varchar(2048),
    image       varchar(255),
    name        varchar(255),
    price       numeric(19, 2),
    stock       int8,
    id_author   int8,
    id_genre    int8,
    primary key (id)
);
create table r_cart_product
(
    id_cart    int8 not null,
    id_product int8 not null
);
create sequence hibernate_sequence start 1000 increment 1;
alter table if exists order_t_products
    add constraint FK2vwpxe6yrr9ccywxwiegsjeuy foreign key (products_id) references product;
alter table if exists order_t_products
    add constraint FK8igpkhnvey39l93k1xr2pkc1d foreign key (order_id) references order_t;
alter table if exists product
    add constraint FKe3yi2ykjch3qavegh08srogmp foreign key (id_author) references author;
alter table if exists product
    add constraint FKrdfx1tx1fmt401ma20qli3xw0 foreign key (id_genre) references genre;
alter table if exists r_cart_product
    add constraint FKlbosjf1g6ad7baqd8r29clmh0 foreign key (id_product) references product;
alter table if exists r_cart_product
    add constraint FKr1mlav6nko1o03809tljxqinp foreign key (id_cart) references cart;